/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;
import java.util.Comparator;

/**
 *
 * @author FARIA
 */
public class JogadorComparator implements Comparator<Jogador> {
  
  	Boolean numero;
	Boolean numero_ascendente;
	Boolean nome_ascendente;

  	public JogadorComparator() {
		this.numero  = true;
		this.numero_ascendente = true;
		this.nome_ascendente = true;
	}
  
	public JogadorComparator(Boolean _numero, Boolean _numero_ascendente, Boolean _nome_ascendente) {
		this.numero  = _numero;
		this.numero_ascendente = _numero_ascendente;
		this.nome_ascendente = _nome_ascendente;
	}

	@Override
	public int compare(Jogador j1, Jogador j2) {

		int retorno;

		if (numero) {
			if (j1.numero == j2.numero) {
				retorno = j1.nome.compareTo(j2.nome);
              
              	if (!nome_ascendente)
                  retorno *= -1;            
			} else {
				retorno = j1.compareTo(j2);
              
              	if (!numero_ascendente)
                  retorno *= -1;
			}
		} else {
			if (j1.nome.equals(j2.nome)) {
				retorno = j1.compareTo(j2);
              
              	if (!numero_ascendente)
                  retorno *= -1;
			} else {
				retorno = j1.nome.compareTo(j2.nome);
              
              	if (!nome_ascendente)
                  retorno *= -1;
			}
		}
		return retorno;
	}
  
}